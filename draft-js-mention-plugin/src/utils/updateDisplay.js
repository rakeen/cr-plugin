import { Modifier, EditorState } from 'draft-js';
import getEntityArray from './getEntityArray';

export const addEntity = (editorState, entity) => {
    const contentStateWithEntity = editorState.getCurrentContent().createEntity(
        entity.type, 'MUTABLE', { ...entity }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const currentSelectionState = editorState.getSelection();
    const mentionTextSelection = currentSelectionState.merge({
        anchorOffset: entity.start,
        focusOffset: entity.end,
    });

    let mentionReplacedContent = Modifier.replaceText(
        editorState.getCurrentContent(),
        mentionTextSelection,
        entity.text,
        null, // no inline style needed
        entityKey
    );
    return mentionReplacedContent;
}

export default (editorState, displayTokens) => {
    let prevEntityArray = getEntityArray(editorState);
    let incompleteEntities = [];
    prevEntityArray.forEach( (e,idx)=>{
        if(e.type==='INCOMPLETE'){
            incompleteEntities.push({
                idx: idx,
                ...e
            });
        }
    });

    incompleteEntities.forEach((e)=>{
        if(displayTokens[e.idx].type!=='INCOMPLETE'){
            editorState = EditorState.push(
                editorState,
                addEntity(editorState, displayTokens[e.idx]),
                'insert-mention',
            );
        }
    });
    return editorState;
};