export default (editorState) => {
    const content = editorState.getCurrentContent();
    const entities = [];
    content.getBlocksAsArray().forEach((block) => {
        let selectedEntity = null;
        block.findEntityRanges(
        (character) => {
            if (character.getEntity() !== null) {
                const entity = content.getEntity(character.getEntity());
                selectedEntity = {
                    entityKey: character.getEntity(),
                    blockKey: block.getKey(),
                    entity: content.getEntity(character.getEntity()),
                };
                return true;
            }
            return false;
        },
        (start, end) => {
            entities.push({ ...selectedEntity, start, end });
        });
    });

    const plainText = content.getPlainText();
    let prev = -1;
    let entityArray = [];
    entities.forEach(entity=>{
        let { type } = entity.entity.getData()
        if(prev+1!==entity.start){
            let temp_a = {
                start: prev+1,
                end: entity.start-1,
                type: 'INCOMPLETE',
                text: plainText.substring(prev+1, entity.start-1)
            };
            entityArray.push(temp_a);
        }
        let temp = {
            start: entity.start,
            end: entity.end,
            type: type,
            text: plainText.substring(entity.start, entity.end)
        };
        entityArray.push(temp);
    });
    return entities;
};