import { Modifier, EditorState } from 'draft-js';
import getSearchText from '../utils/getSearchText';
import getTypeByTrigger from '../utils/getTypeByTrigger';


const getEntities = (editorState, entityType = null) => {
  const content = editorState.getCurrentContent();
  const entities = [];
  content.getBlocksAsArray().forEach((block) => {
    let selectedEntity = null;
    block.findEntityRanges(
      (character) => {
        if (character.getEntity() !== null) {
          const entity = content.getEntity(character.getEntity());
          if (!entityType || (entityType && entity.getType() === entityType)) {
            selectedEntity = {
              entityKey: character.getEntity(),
              blockKey: block.getKey(),
              entity: content.getEntity(character.getEntity()),
            };
            return true;
          }
        }
        return false;
      },
      (start, end) => {
        entities.push({ ...selectedEntity, start, end });
      });
  });
  return entities;
};


/**
 * similar to `addMention()`
 * 
 * given `editorState`, find the `entityList`
 * decide which substring to substitute with the mention.Text  
 * 
 */
export const tokenizeDisplay = (editorState, mention, mentionPrefix, mentionTrigger, entityMutability, suggestions) => {
  if(suggestions.length===1){
    let lastEntity = getEntities(editorState).pop();
    const plainText = editorState.getCurrentContent().getPlainText();

    let _begin = lastEntity ? lastEntity.end + 1 : 0;

    console.log("INSIDE",suggestions);
    let replacableRange = plainText.substr(_begin).trim();
    if(replacableRange.endsWith(suggestions[0].name)) _begin = plainText.length - suggestions[0].name.length;
  

    const contentStateWithEntity = editorState.getCurrentContent().createEntity(
      getTypeByTrigger(mentionTrigger), entityMutability, { mention }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();

    const currentSelectionState = editorState.getSelection();
    let { begin, end } = getSearchText(editorState, currentSelectionState, mentionTrigger);
    begin = _begin;
    console.log("begin: ", lastEntity, begin)
    // get selection of the @mention search text
    const mentionTextSelection = currentSelectionState.merge({
      anchorOffset: begin,
      focusOffset: end,
    });

    let mentionReplacedContent = Modifier.replaceText(
      editorState.getCurrentContent(),
      mentionTextSelection,
      `${mentionPrefix}${mention.name}`,
      null, // no inline style needed
      entityKey
    );

    // If the mention is inserted at the end, a space is appended right after for
    // a smooth writing experience.
    // const blockKey = mentionTextSelection.getAnchorKey();
    // const blockSize = editorState.getCurrentContent().getBlockForKey(blockKey).getLength();
    // if (blockSize === end) {
    //   mentionReplacedContent = Modifier.insertText(
    //     mentionReplacedContent,
    //     mentionReplacedContent.getSelectionAfter(),
    //     ' ',
    //   );
    // }

    const newEditorState = EditorState.push(
      editorState,
      mentionReplacedContent,
      'insert-mention',
    );
    return EditorState.forceSelection(newEditorState, mentionReplacedContent.getSelectionAfter());
  }
  else return editorState;
}



const addMention = (editorState, mention, mentionPrefix, mentionTrigger, entityMutability) => {

  console.log(getEntities(editorState));

  let lastEntity = getEntities(editorState).pop();
  let _begin = lastEntity ? lastEntity.end + 1 : 0;

  const contentStateWithEntity = editorState.getCurrentContent().createEntity(
    getTypeByTrigger(mentionTrigger), entityMutability, { mention }
  );
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();

  const currentSelectionState = editorState.getSelection();
  let { begin, end } = getSearchText(editorState, currentSelectionState, mentionTrigger);
  begin = _begin;
  console.log("begin: ", lastEntity, begin)
  // get selection of the @mention search text
  const mentionTextSelection = currentSelectionState.merge({
    anchorOffset: begin,
    focusOffset: end,
  });

  let mentionReplacedContent = Modifier.replaceText(
    editorState.getCurrentContent(),
    mentionTextSelection,
    `${mentionPrefix}${mention.name}`,
    null, // no inline style needed
    entityKey
  );

  // If the mention is inserted at the end, a space is appended right after for
  // a smooth writing experience.
  const blockKey = mentionTextSelection.getAnchorKey();
  const blockSize = editorState.getCurrentContent().getBlockForKey(blockKey).getLength();
  if (blockSize === end) {
    mentionReplacedContent = Modifier.insertText(
      mentionReplacedContent,
      mentionReplacedContent.getSelectionAfter(),
      ' ',
    );
  }

  const newEditorState = EditorState.push(
    editorState,
    mentionReplacedContent,
    'insert-mention',
  );
  return EditorState.forceSelection(newEditorState, mentionReplacedContent.getSelectionAfter());
};

export default addMention;
